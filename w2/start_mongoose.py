
import os
import socket

# find localhost ip
if os.name != "nt":
    import fcntl
    import struct

    def get_interface_ip(ifname):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(s.fileno(), 0x8915, struct.pack('256s',
                                ifname[:15]))[20:24])

def get_lan_ip():
    ip = socket.gethostbyname(socket.gethostname())
    if ip.startswith("127.") and os.name != "nt":
        interfaces = [
            "eth0",
            "eth1",
            "eth2",
            "wlan0",
            "wlan1",
            "wifi0",
            "ath0",
            "ath1",
            "ppp0",
            ]
        for ifname in interfaces:
            try:
                ip = get_interface_ip(ifname)
                break
            except IOError:
                pass
    return ip

# check if program is available for execution
def which(program):
    import os
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None

localhost_ip = get_lan_ip()

'''
# check if Chrome executable is on the system search path
if which("GoogleChromePortable.exe"):
    os.system("v:\\wwwserver\\mongoose.exe | GoogleChromePortable.exe "+localhost_ip+":8084")
else:
    os.system("v:\\wwwserver\\mongoose.exe")
'''
port = "80"

# use local apps/GoogleChromePortable/GoogleChromePortable.exe
os.system("v:\\wwwserver\\mongoose.exe | v:\\apps\\GoogleChromePortable\\GoogleChromePortable.exe "+localhost_ip+":"+port)

print("start mongoose on "+localhost_ip+":"+port)

