
# users.auth.php
# <?php exit()?>
# Don't modify the lines above
#
# Userfile
#
# Format:
#
# login:passwordhash:Real Name:email:groups,comma,seperated
# smd5 admin:$1$BtiJLaL9$q3Zy/VE8X5VrEMsR.XWdj1:admin:admin@gmail.com:admin,user

admin:$1$BtiJLaL9$q3Zy/VE8X5VrEMsR.XWdj1:admin:admin@gmail.com:admin,user
58B39:2d677ef47c7f9ece096382395a9b6264d5a98b2a:58B39@gm.nfu.edu.tw:58B39@gm.nfu.edu.tw:user
3624D:a93c450b982947f5b3999eba098b5ed1214bb08d:3624D@gm.nfu.edu.tw:3624D@gm.nfu.edu.tw:user
7345B:989b41f7e227924871c07bf7c195f0072a7eb027:7345B@gm.nfu.edu.tw:7345B@gm.nfu.edu.tw:user
358DA:95a16ac4365072726673248f444df7eba75ee1e0:358DA@gm.nfu.edu.tw:358DA@gm.nfu.edu.tw:user
3BC7B:bb6219ca2f108bbbab3fcb66427214ca60a19fe2:3BC7B@gm.nfu.edu.tw:3BC7B@gm.nfu.edu.tw:user
CA55C:70a896fb643df4c19244eefdfe0c3b6068e24c4c:CA55C@gm.nfu.edu.tw:CA55C@gm.nfu.edu.tw:user
D2756:c7fd75ec1dfb78d1ed570e7160e738d03cf49296:D2756@gm.nfu.edu.tw:D2756@gm.nfu.edu.tw:user
368B8:f60809a4518948a58db7c5d6250c9649804f618d:368B8@gm.nfu.edu.tw:368B8@gm.nfu.edu.tw:user
7948A:7be5082ab55621c595dea481d44be9bfc5aceb9c:7948A@gm.nfu.edu.tw:7948A@gm.nfu.edu.tw:user
C65C4:6d179f09c3208d7ee647ecc7ef2382fec8526c68:C65C4@gm.nfu.edu.tw:C65C4@gm.nfu.edu.tw:user
A8964:da61c767b42b77f48638747f90c2f53eb6521c8a:A8964@gm.nfu.edu.tw:A8964@gm.nfu.edu.tw:user
22422:6891153e9320c657d8b11a951583a3f3d3f94fd5:22422@gm.nfu.edu.tw:22422@gm.nfu.edu.tw:user
9A5B4:ce3d965fee0f798491642ddb3dd22ba9d2dced02:9A5B4@gm.nfu.edu.tw:9A5B4@gm.nfu.edu.tw:user
B5A76:644de6fc11d121d957bf808c9b56d9ca4d059a89:B5A76@gm.nfu.edu.tw:B5A76@gm.nfu.edu.tw:user
57388:091f5b9e1a4d1c1d5ea7cce75de24d4b01566250:57388@gm.nfu.edu.tw:57388@gm.nfu.edu.tw:user
8A833:1afac707986f2e384173c677446f1d2a5e898cd3:8A833@gm.nfu.edu.tw:8A833@gm.nfu.edu.tw:user
29AB8:41b6c656f43a0f47657cbd35ea98d2bcec6abeef:29AB8@gm.nfu.edu.tw:29AB8@gm.nfu.edu.tw:user
5ABD2:2e948d36ea4a4bf8658f3a2a661d15672ed0db9b:5ABD2@gm.nfu.edu.tw:5ABD2@gm.nfu.edu.tw:user
86293:e13d11b5cbf8d187fa0979cd0d7f03bf908ac21a:86293@gm.nfu.edu.tw:86293@gm.nfu.edu.tw:user
9DCBC:0025e37ed719f52d0a30b65464de7f65c45fe023:9DCBC@gm.nfu.edu.tw:9DCBC@gm.nfu.edu.tw:user
276DD:b385a60399444c8d209252a409306b87617a70f9:276DD@gm.nfu.edu.tw:276DD@gm.nfu.edu.tw:user
599AA:128496b6c099e3a0636c9ae010453846d9c106c5:599AA@gm.nfu.edu.tw:599AA@gm.nfu.edu.tw:user
9C449:d8ccf636283af15a1a8c5019bc566fa3f3029f96:9C449@gm.nfu.edu.tw:9C449@gm.nfu.edu.tw:user
64236:3d7bf3bc19254caa8a753e3f0640ecd92097ed72:64236@gm.nfu.edu.tw:64236@gm.nfu.edu.tw:user
43AAC:355cd48ab783d5c706f69e17ce4d3a906d235882:43AAC@gm.nfu.edu.tw:43AAC@gm.nfu.edu.tw:user
73B93:cdc7dd45d42e20b08093ec6bebdcaa5e75f14eb7:73B93@gm.nfu.edu.tw:73B93@gm.nfu.edu.tw:user
36283:069fc23c8f5e0de260ea661a380b5f6e645c1349:36283@gm.nfu.edu.tw:36283@gm.nfu.edu.tw:user
7447C:1dfa6eceeb1adf5ad90812f0303a49f1d51ee0ad:7447C@gm.nfu.edu.tw:7447C@gm.nfu.edu.tw:user
C73AB:03f557895599312f51b105bea8c7beba7e229e22:C73AB@gm.nfu.edu.tw:C73AB@gm.nfu.edu.tw:user
8284B:36155aa12ba14f9a5f228c1968433257a72b15ce:8284B@gm.nfu.edu.tw:8284B@gm.nfu.edu.tw:user
62454:cfad59f3ec2a14ab35165cb9bd70912c95d1d438:62454@gm.nfu.edu.tw:62454@gm.nfu.edu.tw:user
44B3C:08b10cf81fb5b427cf5207b1740f786a523fa73c:44B3C@gm.nfu.edu.tw:44B3C@gm.nfu.edu.tw:user
C75CB:6bc91eec32e227f7e403c0a04e77d8d8c24131a5:C75CB@gm.nfu.edu.tw:C75CB@gm.nfu.edu.tw:user
4A3CB:02c4f5bb76ee31308b6fa1840d8c6b36cc2208c3:4A3CB@gm.nfu.edu.tw:4A3CB@gm.nfu.edu.tw:user
7D248:7302dc92744cec96317181dda2f76fff73012153:7D248@gm.nfu.edu.tw:7D248@gm.nfu.edu.tw:user
A7CC4:7d4982682678327d5634124567b797571c1076d3:A7CC4@gm.nfu.edu.tw:A7CC4@gm.nfu.edu.tw:user
99BCC:4e6e5471ffe59aa2642b71e5bf20107a217fa411:99BCC@gm.nfu.edu.tw:99BCC@gm.nfu.edu.tw:user
6DA77:d1f2d06aa7e8728998f4f7231bfb34801541eed4:6DA77@gm.nfu.edu.tw:6DA77@gm.nfu.edu.tw:user
357AB:ea761c7c7751066d42635bdca37b9b9df8b3a7e4:357AB@gm.nfu.edu.tw:357AB@gm.nfu.edu.tw:user
A9525:886cf5d39491b82a810b37dcfc3fd69482b9ee2e:A9525@gm.nfu.edu.tw:A9525@gm.nfu.edu.tw:user
