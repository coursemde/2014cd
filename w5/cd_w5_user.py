import hashlib
import string
import random
 
''' sha1 secure hashes '''
 
# convert user_password into sha1 encoded string
def gen_password(user_password):
    return hashlib.sha1(user_password.encode("utf-8")).hexdigest()
 
# generate random user password
def user_password(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def gen_users_auth_php(stud_list_filename, outputdir, sep):
    # read lines from file
    #lines = open("2b_stud_list.txt", "r", encoding="utf-8").read().splitlines()
    lines = open(stud_list_filename, "r", encoding="utf-8").read().splitlines()
    # we may also need to notice every user with computer generated passwords
    file_header = '''
# users.auth.php
# <?php exit()?>
# Don't modify the lines above
#
# Userfile
#
# Format:
#
# login:passwordhash:Real Name:email:groups,comma,seperated
# smd5 admin:$1$BtiJLaL9$q3Zy/VE8X5VrEMsR.XWdj1:admin:admin@gmail.com:admin,user

admin:$1$BtiJLaL9$q3Zy/VE8X5VrEMsR.XWdj1:admin:admin@gmail.com:admin,user
'''
    outputfile = open(outputdir+"/users.auth.php", "w", encoding="utf-8")
    outputfile.write(file_header)
    for i in range(len(lines)):
        password = user_password(3, "ABCDEFGHJKMNPQRSTUVWXYZ23456789")
        user_account = lines[i].split("\t")[0]+"_gm.nfu.edu.tw"
        computer_generated_password = password
        sha1_password = gen_password(password)
        real_name = lines[i].split("\t")[0]+"@gm.nfu.edu.tw"
        email = lines[i].split("\t")[0]+"@gm.nfu.edu.tw"
        groups = "user"
        '''
        print(lines[i].split("\t")[0], "-", password, "-", gen_password(password), \
                "-", lines[i].split("\t")[1])
        '''
        print(password)
        line_to_write = user_account+sep+sha1_password+sep+real_name+sep+email+sep+groups+"\n"
        outputfile.write(line_to_write)
    outputfile.close()

# call gen_users_auth_php() to generate users_auth.php file
gen_users_auth_php("../w4/2ag1_stud_list.txt", "./", sep=":")
print("done")