import hashlib
import string
import random
 
''' sha1 secure hashes '''
 
# convert user_password into sha1 encoded string
def gen_password(user_password):
    return hashlib.sha1(user_password.encode("utf-8")).hexdigest()
 
# generate random user password
def user_password(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

# add input seperate string and output seperate string
def gen_users_auth_php(stud_list_filename, outputdir, insep, outsep):
    # read lines from file
    lines = open(stud_list_filename, "r", encoding="utf-8").read().splitlines()
    # we may also need to notice every user with computer generated passwords
    file_header = '''
# users.auth.php
# <?php exit()?>
# Don't modify the lines above
#
# Userfile
#
# Format:
#
# login:passwordhash:Real Name:email:groups,comma,seperated
# smd5 admin:$1$BtiJLaL9$q3Zy/VE8X5VrEMsR.XWdj1:admin:admin@gmail.com:admin,user

admin:$1$BtiJLaL9$q3Zy/VE8X5VrEMsR.XWdj1:admin:admin@gmail.com:admin,user
'''
    outputfile = open(outputdir+"/w8_users.auth.php", "w", encoding="utf-8")
    outputfile.write(file_header)
    for i in range(len(lines)):
        user_account = lines[i].split(insep)[0]
        password =  lines[i].split(insep)[1]
        sha1_password = gen_password(password)
        real_name = lines[i].split(insep)[0]+"@gm.nfu.edu.tw"
        email = lines[i].split(insep)[0]+"@gm.nfu.edu.tw"
        groups = "user"

        print(lines[i].split(insep)[0], "-", password, "-", gen_password(password), \
                "-", lines[i].split(insep)[1])

        line_to_write = user_account+outsep+sha1_password+outsep+real_name+outsep+email+outsep+groups+"\n"
        outputfile.write(line_to_write)
    outputfile.close()

# call gen_users_auth_php() to generate users_auth.php file
gen_users_auth_php("w8list.txt", "./", insep=" , ", outsep=":")
print("done")
'''
執行結果:
    
經過手動比對, 資料正確無誤.

58B39 - 9J35UAVM - 2d677ef47c7f9ece096382395a9b6264d5a98b2a - 9J35UAVM
3624D - QANF34CW - a93c450b982947f5b3999eba098b5ed1214bb08d - QANF34CW
7345B - 3PAFXKWZ - 989b41f7e227924871c07bf7c195f0072a7eb027 - 3PAFXKWZ
358DA - E6RJFKW4 - 95a16ac4365072726673248f444df7eba75ee1e0 - E6RJFKW4
3BC7B - HG2ASNYH - bb6219ca2f108bbbab3fcb66427214ca60a19fe2 - HG2ASNYH
CA55C - XUZTHWQK - 70a896fb643df4c19244eefdfe0c3b6068e24c4c - XUZTHWQK
D2756 - UHK2W3D2 - c7fd75ec1dfb78d1ed570e7160e738d03cf49296 - UHK2W3D2
368B8 - A5QGYA6W - f60809a4518948a58db7c5d6250c9649804f618d - A5QGYA6W
7948A - PF278WDQ - 7be5082ab55621c595dea481d44be9bfc5aceb9c - PF278WDQ
C65C4 - GT4KBCXU - 6d179f09c3208d7ee647ecc7ef2382fec8526c68 - GT4KBCXU
A8964 - 3RPQSW2U - da61c767b42b77f48638747f90c2f53eb6521c8a - 3RPQSW2U
22422 - 7E57K7F3 - 6891153e9320c657d8b11a951583a3f3d3f94fd5 - 7E57K7F3
9A5B4 - 4MVKRE5Z - ce3d965fee0f798491642ddb3dd22ba9d2dced02 - 4MVKRE5Z
B5A76 - 3DTAAHUF - 644de6fc11d121d957bf808c9b56d9ca4d059a89 - 3DTAAHUF
57388 - NP39FGXR - 091f5b9e1a4d1c1d5ea7cce75de24d4b01566250 - NP39FGXR
8A833 - C7DNBHCQ - 1afac707986f2e384173c677446f1d2a5e898cd3 - C7DNBHCQ
29AB8 - 6KMGK73Z - 41b6c656f43a0f47657cbd35ea98d2bcec6abeef - 6KMGK73Z
5ABD2 - PV5FH722 - 2e948d36ea4a4bf8658f3a2a661d15672ed0db9b - PV5FH722
86293 - PJ69FBMS - e13d11b5cbf8d187fa0979cd0d7f03bf908ac21a - PJ69FBMS
9DCBC - U5HR6QR8 - 0025e37ed719f52d0a30b65464de7f65c45fe023 - U5HR6QR8
276DD - URE9FNWD - b385a60399444c8d209252a409306b87617a70f9 - URE9FNWD
599AA - X2P6CTXF - 128496b6c099e3a0636c9ae010453846d9c106c5 - X2P6CTXF
9C449 - DKRN3V59 - d8ccf636283af15a1a8c5019bc566fa3f3029f96 - DKRN3V59
64236 - 86UWN3E9 - 3d7bf3bc19254caa8a753e3f0640ecd92097ed72 - 86UWN3E9
43AAC - MNCJZCAX - 355cd48ab783d5c706f69e17ce4d3a906d235882 - MNCJZCAX
73B93 - QX945VJJ - cdc7dd45d42e20b08093ec6bebdcaa5e75f14eb7 - QX945VJJ
36283 - M3MQGUXD - 069fc23c8f5e0de260ea661a380b5f6e645c1349 - M3MQGUXD
7447C - TQZVDKPT - 1dfa6eceeb1adf5ad90812f0303a49f1d51ee0ad - TQZVDKPT
C73AB - MSP4GPPX - 03f557895599312f51b105bea8c7beba7e229e22 - MSP4GPPX
8284B - XMT8W9RD - 36155aa12ba14f9a5f228c1968433257a72b15ce - XMT8W9RD
62454 - SD4C7V89 - cfad59f3ec2a14ab35165cb9bd70912c95d1d438 - SD4C7V89
44B3C - 636DBRJC - 08b10cf81fb5b427cf5207b1740f786a523fa73c - 636DBRJC
C75CB - M66RMMQ2 - 6bc91eec32e227f7e403c0a04e77d8d8c24131a5 - M66RMMQ2
4A3CB - FF485EQ4 - 02c4f5bb76ee31308b6fa1840d8c6b36cc2208c3 - FF485EQ4
7D248 - FGJHQDAS - 7302dc92744cec96317181dda2f76fff73012153 - FGJHQDAS
A7CC4 - R47AHA4Y - 7d4982682678327d5634124567b797571c1076d3 - R47AHA4Y
99BCC - RNQYZGQZ - 4e6e5471ffe59aa2642b71e5bf20107a217fa411 - RNQYZGQZ
6DA77 - HRCDP9D8 - d1f2d06aa7e8728998f4f7231bfb34801541eed4 - HRCDP9D8
357AB - CKB4Q2EC - ea761c7c7751066d42635bdca37b9b9df8b3a7e4 - CKB4Q2EC
A9525 - JDVX75ST - 886cf5d39491b82a810b37dcfc3fd69482b9ee2e - JDVX75ST
'''