"# This is my README" 

同步規劃:

Bitbucket: https://bitbucket.org/cmsimply/public_cmsimply

Bitbucket 免費帳號特性:

可以自由將倉儲轉為 public 或 private, 沒有限制

每一倉儲最多只能有 5 個 collaborators

OpenShift: http://cdproject-coursemdetw.rhcloud.com/

OpenShift 免費帳號特性:

支援各種網際程式雲端部署, 但是每一帳號至多只能建 3 個應用程式

接下來要加上與 Github 的全班協同倉儲進行同步

Github 免費帳號特性:

免費帳號只能使用 public 倉儲

沒有限制 collaborators 個數, 適合多協同人員執行開放專案使用
