\contentsline {subsubsection}{更新記錄}{i}{section*.1}
\contentsline {chapter}{\numberline {第1章\hspace {0.3em}}環境}{1}{chapter*.2}
\contentsline {section}{\numberline {1.1}為什麼要自己排版？}{1}{section*.3}
\contentsline {section}{\numberline {1.2}為什麼要用？}{1}{section*.3}
\contentsline {subsection}{\numberline {1.2.1}動手之前}{2}{section*.4}
\contentsline {section}{\numberline {1.3}軟體工具}{2}{section*.4}
\contentsline {subsection}{\numberline {1.3.1}作業系統}{2}{section*.5}
\contentsline {subsection}{\numberline {1.3.2}T\kern -.1667em\lower .5ex\hbox {E}\kern -.125emX\spacefactor \@m 發行版本}{2}{section*.5}
\contentsline {subsection}{\numberline {1.3.3}PDF閱讀器}{3}{section*.5}
\contentsline {subsection}{\numberline {1.3.4}離線備份}{3}{section*.5}
\contentsline {section}{\numberline {1.4}版本管理}{3}{section*.5}
\contentsline {subsection}{\numberline {1.4.1}理想的工作流程}{3}{section*.6}
\contentsline {subsection}{\numberline {1.4.2}現實的工作流程}{3}{section*.6}
\contentsline {section}{\numberline {1.5}\texttt {.tex} 檔組織}{3}{section*.6}
\contentsline {chapter}{\numberline {第2章\hspace {0.3em}}版式}{4}{section*.6}
\contentsline {section}{\numberline {2.1}紙張大小}{4}{section*.7}
\contentsline {section}{\numberline {2.2}版心大小}{4}{section*.7}
\contentsline {section}{\numberline {2.3}頁眉與頁腳}{5}{section*.7}
\contentsline {section}{\numberline {2.4}中文字體}{6}{section*.7}
\contentsline {subsection}{\numberline {2.4.1}不要使用中文斜體}{6}{section*.8}
\contentsline {section}{\numberline {2.5}英文字體}{6}{section*.8}
\contentsline {subsection}{\numberline {2.5.1}羅馬字體}{6}{section*.9}
\contentsline {subsection}{\numberline {2.5.2}無襯線字體}{6}{section*.9}
\contentsline {subsection}{\numberline {2.5.3}等寬字體}{7}{section*.9}
\contentsline {subsection}{\numberline {2.5.4}特殊字體}{7}{section*.9}
\contentsline {section}{\numberline {2.6}行距與段距}{7}{section*.9}
\contentsline {section}{\numberline {2.7}整段代碼}{7}{section*.9}
\contentsline {chapter}{\numberline {第3章\hspace {0.3em}}樣式}{8}{section*.9}
\contentsline {section}{\numberline {3.1}轉義字元}{8}{section*.10}
\contentsline {section}{\numberline {3.2}斜體}{8}{section*.10}
\contentsline {section}{\numberline {3.3}列表}{8}{section*.10}
\contentsline {section}{\numberline {3.4}章節標題}{8}{section*.10}
\contentsline {subsection}{\numberline {3.4.1}編號}{8}{section*.11}
\contentsline {section}{\numberline {3.5}圖表編號}{8}{section*.11}
\contentsline {section}{\numberline {3.6}註腳}{9}{section*.11}
\contentsline {subsection}{\numberline {3.6.1}編號}{9}{section*.12}
\contentsline {subsection}{\numberline {3.6.2}置底}{9}{section*.12}
\contentsline {section}{\numberline {3.7}參考文獻}{10}{section*.12}
\contentsline {chapter}{\numberline {第4章\hspace {0.3em}}工具}{11}{section*.12}
\contentsline {subsubsection}{下載}{11}{section*.12}
\contentsline {section}{\numberline {4.1}統計中文字數}{11}{section*.13}
\contentsline {section}{\numberline {4.2}PDF內容對比（dif\/f\,）}{11}{section*.13}
\contentsline {section}{\numberline {4.3}PDF截取}{11}{section*.13}
\contentsline {section}{\numberline {4.4}PDF頁碼編號}{11}{section*.13}
\contentsline {section}{\numberline {4.5}PDF剪裁（crop）}{11}{section*.13}
\contentsline {section}{\numberline {4.6}PDF拼接（two-up）}{12}{section*.13}
\contentsline {section}{\numberline {4.7}PDF小冊子（booklet）}{13}{section*.13}
\contentsline {section}{\numberline {4.8}PDF字型內嵌}{14}{section*.13}
\contentsline {chapter}{\numberline {第5章\hspace {0.3em}}插圖}{15}{section*.13}
\contentsline {section}{\numberline {5.1}繪圖軟體}{15}{section*.14}
\contentsline {subsection}{\numberline {5.1.1}Graphviz}{15}{section*.14}
\contentsline {subsection}{\numberline {5.1.2}gpic}{15}{section*.14}
\contentsline {subsection}{\numberline {5.1.3}METAPOST}{15}{section*.14}
\contentsline {subsection}{\numberline {5.1.4}Visio}{15}{section*.14}
\contentsline {subsection}{\numberline {5.1.5}Word}{15}{section*.14}
\contentsline {subsection}{\numberline {5.1.6}Excel}{15}{section*.14}
\contentsline {subsection}{\numberline {5.1.7}Gnuplot}{15}{section*.14}
\contentsline {section}{\numberline {5.2}待續……}{15}{section*.14}
