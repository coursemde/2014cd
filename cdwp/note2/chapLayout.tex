\chapter{版式}

本文使用的長度單位：
1英寸 = 25.4mm = 72.27pt \footnote{pt是point的縮寫，中文叫“磅”。} = 72bp。
注意 \TeX 定義的pt和PostScript（亦即Adobe系列軟體）定義的pt的長度不一樣，
本書以 \TeX 定義的pt為准，Post\-Script的基本長度單位寫為bp（big point）。

\section{紙張大小} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

書籍的紙張大小通常異於常用的打印紙大小（A4或B5），
因此用 \LaTeX 排版書籍的第一步是設置好紙張大小。
一般常見的IT圖書的開本（成品書尺寸，可以用尺子量出來）是 185mm $\times$ 230mm，
即“國際18開”；另外一種常見開本是185mm $\times$ 260mm，即“16開”。
本文以“國際18開”為例。
我們通常可以用 \fn{geometry} 宏包來設置紙張和版心尺寸，
例子見 \S \ref{sec:textbody}。
\index{宏包!geometry@\fn{geometry}|(}
%\index{開本}

\section{版心大小} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sec:textbody}

“版心”即正文區，不包含頁眉和頁腳
\footnote{這是本文的定義，也有將版心定義為包含頁眉和頁腳的。}。
版心的大小可以這樣計算：
通常正文字型大小是10pt
\footnote{比五號（10.5pt）字略小，比小五號字（9pt）大。}，
一行按39個漢字計算，
%\footnote{。一般不宜超過40個。}
那麼行寬是 390pt。
行寬是正文字型大小的整數倍，這樣中文間距不會無故拉寬。
行寬不宜過大，否則閱讀的時候容易讀串列；
也不宜過小，否則一行排不下80列代碼。一般而言，
36 \textasciitilde\ 42字比較適宜，本文定為39字，數數上一行:-)。
\mybooktitle 一行是37個漢字，
因為這本書厚達600頁，如果版心太寬容易影響閱讀訂口的文字。

對於10pt的正文字體，\LaTeX 預設的行距
\footnote{行距指的是英文基線（baseline）之間的距離，即中文漢字底部之間的距離，
不是兩行之間的空白。} 是12pt，這對於英文是合適的
\footnote{因為英文文本多是小寫字母，字高遠小於10pt。}，
但是對於中文則顯得太密了。因此 \CTeX 宏包將 \mn{baselinestretch} 定義為 1.3，
\index{命令!baselinestretch@\mn{baselinestretch}}
這樣行距是\linebreak 1.3$\times$12pt = 15.6pt，閱讀起來就比較順眼了。
如果一頁排34行字，那麼版心的高度大約是 \nolinebreak
\footnote{這裡說“大約”，因為第一行上方似乎不必留出多餘的空白。} 34$\times$15.6pt=530.4pt，
本文取 530pt
\footnote{對於技術類圖書，通常一個自然段不會太長，
一頁之內幾乎總是會遇到分段（整段代碼、圖表、章節標題）的情況，
因此版心高度不必嚴格是行距的整數倍。} 。

綜上，對於39字 $\times$ 34行的版心，其尺寸是 390pt $\times$ 530pt，約合 137mm $\times$ 186mm。見下圖示意。

%\vspace{1ex}
\centerline{\includegraphics{paper.eps}}

知道了紙張和版心尺寸，剩下的就交給 \fn{geometry} 宏包。
它同時會設置生成的PDF的紙張尺寸。
例如 \myurl{examples/paper.tex}。
\index{宏包!geometry@\fn{geometry}|)}

\begin{Codex}[label=examples/paper.tex]
\documentclass[10pt,fancyhdr,UTF8]{ctexbook}
\usepackage[centering,paperwidth=180mm,paperheight=230mm,%
            body={390pt,530pt},showframe]{geometry}
\end{Codex}

注意這裡把紙張寬度設為 180mm，這是考慮到裝訂的位置，
這樣在電腦上預覽的時候左右空白更貼近實際印刷的效果。
我們也不必關心版心在紙張中的上下左右位置，居中即可，
在印刷的時候有專人負責拼版。當然在正式排版的時候要去掉 \fn{showframe} 選項。

另外，打印紙一般不會剛好和書籍開本一樣大，
要想在書印出來之前感受版面效果，
可以列印在A4紙上，但需要將書頁框出來，可用 \fn{crop} 宏包。
例如 \myurl{examples/paper-crop.tex}。
\index{宏包!crop@\fn{crop}}

\begin{Code}
$ diff -u paper.tex paper-crop.tex
--- paper.tex           2012-12-29 14:03:02.000 +0800
+++ paper-crop.tex      2012-12-29 14:03:02.000 +0800
 \documentclass[10pt,fancyhdr,UTF8]{ctexbook}
 \usepackage[centering,paperwidth=180mm,paperheight=230mm,%
             body={390pt,530pt},showframe]{geometry}
+\usepackage[a4,center,frame,color=blue]{crop}
\end{Code}

\section{頁眉與頁腳} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

書籍排版不是以一頁（一面）為單位，而是以翻開之後的兩面（左右頁）為單位。
\LaTeX 預設會設法讓左右頁的內容一樣多，即左右頁的最後一行位於同一高度。
有時候這會造成難看的版面，特別是有可能留下過多段間空白。
一般可以用 \mn{raggedbottom} 命令來取消這一設定。
\index{命令!raggedbottom@\mn{raggedbottom}}

頁眉的外側（切口）是頁碼，內側（訂口）是章節名稱，通常是左頁（偶數頁）放章名，
右頁（奇數頁）放節名
\footnote{\mybooktitle p.393 的內側頁眉為空，因為該章第1節出現在 p.394。}。
頁腳通常可以放書名，這樣即便複印其中一面也容易知道出自何處%
（本文把作者姓名和網址也放到頁腳，便於網上傳播）。
典型的安排如下圖所示。

\vspace{1ex}
\centerline{\fbox{\includegraphics[page=2,scale=0.9]{header.pdf}}%
\quad\fbox{\includegraphics[page=3,scale=0.9]{header.pdf}}}

\vspace{1ex}
我們一般用 \fn{fancyhdr} 宏包來設置頁眉和頁腳，常用的設置如下。
其中 \fn{RE} 表示偶數頁\linebreak（Even）右側（Right），
\fn{LO} 表示奇數頁（Odd）左側（Left），
\fn{LE}、\fn{RO} 的意思想必讀者能舉一反三猜出來。
\index{宏包!fancyhdr@\fn{fancyhdr}}

\begin{Code}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[RE]{\normalfont\small\rmfamily\nouppercase{\leftmark}}
\fancyhead[LO]{\normalfont\small\rmfamily\nouppercase{\rightmark}}
\fancyhead[LE,RO]{\thepage}
\fancyfoot[LE,LO]{\small Book Title}
\end{Code}

通常每章的第一頁沒有頁眉，頁碼放到頁腳中央，即 \fn{plain} 頁面格式，如下圖所示。

\vspace{1ex}
\centerline{\fbox{\includegraphics[page=1,scale=0.9]{header.pdf}}}

\section{中文字體} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

首先，適合螢幕閱讀的字體不一定適合印刷書籍。
原因可能有兩點，一是書籍的印刷解析度遠高於螢幕；
二是螢幕會主動發光，而書籍是被動反光。
其次，中文字體用三四種（宋、黑、楷）就足夠了，不要太花哨。
印刷用的正文字體可用方正書宋或華康簡宋，
視出版社的字體授權情況而定。
螢幕閱讀可用Windows中文字體或Adobe中文字體。
注意某些Adobe中文字體的標點符號位置不正確，
例如Adobe楷體的全形冒號和分號上下居中，而不是位於左下角（
\setCJKfamilyfont{adobekai}{Adobe Kaiti Std}
{\CJKfamily{adobekai}子曰：“食不厭精；膾不厭細。”}），
使用時需要注意。
另外，方正書宋和華康簡宋缺少某些繁體字（例如“碁”），可以臨時改換為Adobe宋體。
例如
\begin{Code}
\setCJKfamilyfont{adobesong}{Adobe Song Std}
《C++程式設計規範（繁體版）》由{\CJKfamily{adobesong} 碁峰}出版社出版。
\end{Code}

\subsection{不要使用中文斜體}
\label{subsec:noChineseItalic}
中文斜體是非常難看的，千萬不要用。
為了突出中文術語，可以用\textbf{黑體}，例如\textbf{反及閘}。
傳統科技書籍也常用{\kaishu 楷體}表示術語和強調，不過黑體目前似乎更流行一些。


\section{英文字體} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
英文字體可選擇的範圍就大多了，可參考
The \LaTeX\ Font Catalogue \footnote{\myurl{http://www.tug.dk/FontCatalogue/}}。

\subsection{羅馬字體}
不要使用 {\fontfamily{lmr}\selectfont Computer Modern Roman}，
它筆劃太細而且襯線略顯誇張。
英文羅馬字體一般可以選 {\fontspec{Times New Roman} Times Roman} 
\footnote{實際的字體名是 \fn{Nimbus Roman No9 L} 或 \fn{TeX Gyre Termes} 或 \fn{Times New Roman} 等。}
或 {\fontspec{URW Palladio L} Palatino}
\footnote{實際的字體名是 \fn{URW Palladio L} 或 \fn{TeX Gyre Pagella} 等。}。
例如
\begindot
\item {\fontspec{TeX Gyre Termes} C++ is a general-purpose programming language. [Times Roman]}
\item {\fontspec{TeX Gyre Pagella} C++ is a general-purpose programming language. [Palatino]}
\myenddot

\subsection{無襯線字體}

一般用於章節標題和程式設計語言關鍵字，例如“\kw{this} 指針”、
“\kw{mutable} 成員變數”。由於它出現的機會少，用什麼字體其實無所謂，
默認的 \kw{Computer Modern Sans Serif} 就行。
也可以換為 {\fontspec{Nimbus Sans L} Helvetica} 
\footnote{實際的字體名是 \fn{Nimbus Sans L} 或 \fn{TeX Gyre Heros} 或 \fn{Arial} 等。}。

\subsection{等寬字體}
一般用於代碼，包括變數名、類名等等。
不要使用 {\fontspec{Courier New} Courier New}，它太細而且太寬。
可以用\LaTeX 默認的 {\fontspec{Latin Modern Mono} Computer Modern Typewriter}
\footnote{實際字體名是 \fn{Latin Modern Mono}。} 
或 {\fontspec{Inconsolata} Inconsolata}。後者要窄一些，但是雙引號略彎。例如
\begindot
\item[] {\small \fontspec{Latin Modern Mono} printf("Hello \%s\bs n", name);} [cmtt]
\item[] {\small \fontspec[Mapping=tex-text-tt]{Inconsolata} printf("Hello \%s\bs n", name);} [Inconsolata]
\myenddot

注意如果要使用 Inconsolata 字體來排版代碼，需要防止 \LaTeX 替換其中的字元，
例如 {\fontspec[Mapping=tex-text-tt]{Inconsolata} operator<<()} 變成 {\fontspec{Inconsolata} operator<<()} 等。
可以通過 \fn{fontspec} 宏包的 \fn{Mapping} 選項來做到這一點，
並搭配合適的 TECkit 映射文件
\footnote{見本文源碼目錄中的 \fn{tex-text-tt.map}，需要用 \fn{teckit_compile} 工具編譯為 \fn{{}.tec} 文件。}。

如果要排版大量Java代碼（一行可長達100列），可以考慮用窄的無襯線等寬字體，
如 TheSansMono Condensed，但似乎沒有免費版本。
另外，一些英文書籍也用 {\small \fontspec{Lucida Sans Typewriter}Lucida Sans Typewriter} 來排版代碼，可以從Sun JDK中找到。

\subsection{特殊字體}
URL可以用窄字體，以節省空間。
例如 {\fontspec{Ubuntu Condensed}Ubuntu Condensed} 或者 \myurl{PT Sans Narrow}。
特殊術語可以用稍微誇張一點的字體凸顯，例如“{\fontspec{URW Gothic L} Extract Method} 重構”和
“{\fontspec{URW Gothic L} Observer} 模式”。

\section{行距與段距}
中文區分自然段的方法有三種，傳統的方案是段首空兩格（indent），
現代的方案是兩段之間增加空白（\mn{parskip}），
第三種方案是同時使用前兩者。本文採用傳統方案，
\mybooktitle 一書採用第三種方案。
設置\mn{parskip} 的時候要小心，它會影響所有段落之前的空白，
包括章節標題、圖表、圖表編號等等。
\index{命令!parskip@\mn{parskip}}

\section{整段代碼}
\index{宏包!fancyvrb@\fn{fancyvrb}}
用等寬字體排版代碼一般可用 \fn{fancyvrb} 宏包，然後定義自己的\fn{Code} 環境，
字型大小9pt，行距0.9，左邊留出3mm。
\begin{Code}
\DefineVerbatimEnvironment{Code}{Verbatim}%
  {fontsize=\small,baselinestretch=0.9,xleftmargin=3mm}
\end{Code}

用 {\fontspec{Latin Modern Mono} Computer Modern Typewriter} 字體，版心寬度390pt時，一行可排80列。
\begin{Verbatim}[fontsize=\small,baselinestretch=0.9,xleftmargin=3mm]
\begin{Code}
12345678901234567890123456789012345678901234567890123456789012345678901234567890
         1         2         3         4         5         6         7         8
\end{Code}
\end{Verbatim}

用 {\fontspec{Inconsolata} Inconsolata} 字體，版心寬度為390pt時，一行可排84列。
版心寬度為370pt時可排80列。
%\setmonofont[AutoFakeBold=1.6,AutoFakeSlant=0.17,Mapping=tex-text-tt]{Inconsolata}
\makeatletter
\@namedef{FV@fontfamily@inconsolata}{%
  \def\FV@FontScanPrep{}%
  \def\FV@FontFamily{\fontspec{Inconsolata}}}
\makeatother
\begin{Code}[fontfamily=inconsolata]
123456789012345678901234567890123456789012345678901234567890123456789012345678901234
         1         2         3         4         5         6         7         8
\end{Code}

另外還可以定義\fn{Codex} 環境，用於排版帶標題（檔案名）的代碼。

\begin{Code}
\DefineVerbatimEnvironment{Codex}{Verbatim}%
  {fontsize=\small,baselinestretch=0.9,xleftmargin=3mm,%
  frame=lines,labelposition=all,framesep=5pt}
\end{Code}

我對 \fn{fancyvrb} 宏包有一些改動，見 \fn{verbatim.cls}。
最終效果如下：
\begin{Codex}[label=hello.c,numbers=left]
int main()
{
  printf("Hello, world.\n");
}
\end{Codex}
