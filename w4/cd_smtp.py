import smtplib
from email.mime.text import MIMEText  
from email.header import Header
 
# 利用 gmail smtp 功能寄信
server = smtplib.SMTP('smtp.gmail.com:587')
 
server.ehlo()
server.starttls()
server.login('電子郵箱', '密碼')
message= MIMEText('信件內文 http://www.google.com','plain','UTF-8')
#message= MIMEText('信件內文<br /><a href="http://www.google.com">google</a>','html','UTF-8')
message['Subject'] = Header("信件標題", 'UTF-8') 
server.sendmail("from@email", "to@email", message.as_string())
server.quit()
# 若設定　server.set_debuglevel(10), 則可以利用 prin(server.quit()) 印出詳細的協定溝通過程.